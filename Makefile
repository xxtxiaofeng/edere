CC = gcc
SRC = src/*.c
DEFS =  -DGCR_API_SUBJECT_TO_CHANGE

X11LIB = `pkg-config --cflags --libs x11`
GTKLIB = `pkg-config --cflags --libs gtk+-3.0 gcr-3 webkit2gtk-4.0`
LINKS = $(X11LIB) $(GTKLIB) -lgthread-2.0

all:
	mkdir -p bin
	$(CC) $(SRC) -o bin/edere $(LINKS) $(DEFS) -Wall -Wextra